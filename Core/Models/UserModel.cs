﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Models
{
    public class UserModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public byte[] PasswordHash { get; set; }

        [Required]
        public byte[] PasswordSalt { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public DateTime? InsertDate { get; set; }

        public virtual ICollection<BlogModel> Blogs { get; set; }

        public virtual ICollection<CommentModel> Comments { get; set; }
    }
}
