﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models
{
    public class BlogModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public string ImageExtension { get; set; }

        [Required]
        public DateTime? InsertDate { get; set; }

        [Required]
        public DateTime? UpdateDate { get; set; }

        [ForeignKey("UserId")]
        public virtual UserModel User { get; set; }

        public virtual ICollection<CommentModel> Comments { get; set; }
    }
}
