﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Models
{
    public class CommentModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int BlogId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public DateTime? InsertDate { get; set; }

        [ForeignKey("BlogId")]
        public virtual BlogModel Blog { get; set; }

        [ForeignKey("UserId")]
        public virtual UserModel User { get; set; }
    }
}
