﻿using System;

namespace Core
{
    public static class ExtensionMethods
    {
        public static string ToBlogString(this DateTime? date)
        {
            if (date == null)
            {
                throw new NullReferenceException();
            }

            return date.Value.ToString("MMMM dd yyyy HH:mm:ss");
        }
    }
}
