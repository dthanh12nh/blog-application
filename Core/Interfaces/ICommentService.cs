﻿using Core.Models;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface ICommentService
    {
        Task<CommentModel> Add(CommentModel model);
    }
}
