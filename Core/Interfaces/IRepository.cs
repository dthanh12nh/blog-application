﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        DbSet<TEntity> DbSet { get; set; }

        IQueryable<TEntity> GetAll();
        Task<TEntity> Add(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Delete(TEntity model);
        Task<TEntity> First(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        Task<int> SaveChanges();
    }
}
