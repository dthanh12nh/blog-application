﻿using Core.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IBlogService
    {
        Task<BlogModel> GetById(int id);
        (IQueryable<BlogModel> blogs, int totalBlogs) Get(int blogsPerPage, int page);
        Task<BlogModel> Add(BlogModel model);
        BlogModel Update(BlogModel model);
        BlogModel Delete(int id);
    }
}
