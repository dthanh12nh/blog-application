﻿using Core.Models;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IUserService
    {
        Task<UserModel> GetById(int id);
        UserModel GetByUserName(string userName);
        Task<UserModel> Add(UserModel model, string password);
        Task<UserModel> Authenticate(string userName, string password);
    }
}
