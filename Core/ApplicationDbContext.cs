﻿using Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Core
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<UserModel> Users { get; set; }
        public DbSet<BlogModel> Blogs { get; set; }
        public DbSet<CommentModel> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserModel>()
                .Property(b => b.InsertDate)
                .HasDefaultValueSql("getdate()");

            builder.Entity<BlogModel>()
                .Property(b => b.InsertDate)
                .HasDefaultValueSql("getdate()");

            builder.Entity<BlogModel>()
                .Property(b => b.UpdateDate)
                .HasDefaultValueSql("getdate()");

            builder.Entity<CommentModel>()
                .Property(b => b.InsertDate)
                .HasDefaultValueSql("getdate()");

            builder.Entity<UserModel>()
                .HasMany(m => m.Comments)
                .WithOne(m => m.User)
                .HasForeignKey(m => m.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
