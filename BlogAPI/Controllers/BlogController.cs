﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using System.IO;
using Core.Interfaces;
using Core;
using BlogAPI.Models.Blog;
using Core.Models;

namespace BlogAPI.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class BlogController : Controller
    {
        private readonly IBlogService _blogService;
        private readonly IMapper _mapper;

        public BlogController(IBlogService blogService, IMapper mapper)
        {
            _blogService = blogService;
            _mapper = mapper;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Get(int blogsPerPage, int page = 1)
        {
            var result = _blogService.Get(blogsPerPage, page);

            return Ok(new
            {
                page,
                blogsPerPage,
                result.totalBlogs,
                blogs = result.blogs.Select(m => new
                {
                    id = m.Id,
                    title = m.Title,
                    description = m.Description,
                    content = m.Content,
                    imageUrl = GetImageUrl(m.Id, m.ImageExtension),
                    insertDate = m.InsertDate.ToBlogString(),
                    userId = m.UserId
                })
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Read(int id)
        {
            var blog = await _blogService.GetById(id);

            return Ok(new
            {
                id = blog.Id,
                userId = blog.UserId,
                title = blog.Title,
                description = blog.Description,
                content = blog.Content,
                imageUrl = GetImageUrl(blog.Id, blog.ImageExtension),
                insertDate = blog.InsertDate.ToBlogString(),
                user = new
                {
                    id = blog.User.Id,
                    userName = blog.User.UserName,
                    fullName = blog.User.FullName,
                    insertDate = blog.User.InsertDate.ToBlogString()
                },
                Comments = blog.Comments?.OrderByDescending(m => m.InsertDate).Select(m => new
                {
                    id = m.Id,
                    content = m.Content,
                    insertDate = m.InsertDate.ToBlogString(),
                    user = m.User != null ? new
                    {
                        id = m.User.Id,
                        userName = m.User.UserName,
                        fullName = m.User.FullName,
                        insertDate = m.User.InsertDate.ToBlogString()
                    } : null
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddBlogViewModel model)
        {
            var blog = new BlogModel
            {
                Title = model.Title,
                Description = model.Description,
                Content = model.Content,
                ImageExtension = Path.GetExtension(model.Image.FileName),
                UserId = int.Parse(User.Identity.Name)
            };

            var result = await _blogService.Add(blog);

            var file = model.Image;
            var path = Path.Combine(Directory.GetCurrentDirectory(), $"wwwroot\\Images\\{result.Id}{Path.GetExtension(file.FileName)}");
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return Ok(new
            {
                id = result.Id,
                title = result.Title,
                description = model.Description,
                content = result.Content,
                insertDate = result.InsertDate.ToBlogString(),
                imageUrl = GetImageUrl(result.Id, result.ImageExtension),
                userId = result.UserId
            });
        }

        [HttpPut]
        public async Task<IActionResult> Edit(EditBlogViewModel model)
        {
            var originalModel = await _blogService.GetById(model.Id);

            if (model.Image != null && model.Image.Length > 0)
            {
                originalModel.ImageExtension = Path.GetExtension(model.Image.FileName);
            }

            originalModel.Title = model.Title;
            originalModel.Content = model.Content;

            ModelState.Clear();
            TryValidateModel(originalModel);

            if (ModelState.IsValid)
            {
                var result = _blogService.Update(originalModel);

                var path = Path.Combine(Directory.GetCurrentDirectory(), $"wwwroot\\Images\\{result.Id}{Path.GetExtension(model.Image.FileName)}");
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await model.Image.CopyToAsync(stream);
                }

                return Ok(new
                {
                    id = result.Id,
                    title = result.Title,
                    description = model.Description,
                    content = result.Content,
                    insertDate = result.InsertDate.ToBlogString(),
                    imageUrl = GetImageUrl(result.Id, result.ImageExtension),
                    userId = result.UserId
                });
            }

            return BadRequest(ModelState);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var result = _blogService.Delete(id);

            return Ok(new
            {
                id = result.Id,
                title = result.Title,
                content = result.Content,
                description = result.Description,
                insertDate = result.InsertDate.ToBlogString(),
                imageUrl = GetImageUrl(result.Id, result.ImageExtension),
                userId = result.UserId
            });
        }

        private string GetImageUrl(int postId, string imageExtension)
        {
            return Path.Combine($"/Images/{postId}{imageExtension}");
        }
    }
}
