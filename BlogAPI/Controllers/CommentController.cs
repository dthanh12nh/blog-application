﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BlogAPI.Models.Comment;
using Core.Interfaces;
using Core.Models;
using Core;

namespace BlogAPI.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody]AddCommentViewModel model)
        {
            var comment = new CommentModel
            {
                UserId = int.Parse(User.Identity.Name),
                BlogId = model.BlogId.Value,
                Content = model.Content
            };

            var result = await _commentService.Add(comment);

            return Ok(new
            {
                id = result.Id,
                userId = result.UserId,
                blogId = result.BlogId,
                content = result.Content,
                insertDate = result.InsertDate.ToBlogString(),
                user = new
                {
                    id = result.User.Id,
                    userName = result.User.UserName,
                    fullName = result.User.FullName,
                    insertDate = result.User.InsertDate.ToBlogString()
                }
            });
        }
    }
}
