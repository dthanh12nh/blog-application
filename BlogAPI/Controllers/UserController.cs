﻿using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Core.Interfaces;
using Core;
using BlogAPI.Models.User;
using Core.Models;

namespace BlogAPI.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly AppSetting _appSetting;

        public UserController(IUserService userService, IOptions<AppSetting> appSetting)
        {
            _userService = userService;
            _appSetting = appSetting.Value;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginViewModel model)
        {
            try
            {
                var user = await _userService.Authenticate(model.UserName, model.Password);

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSetting.Secret);
                var tokenDescripter = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, user.Id.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescripter);
                var tokenString = tokenHandler.WriteToken(token);

                return Ok(new
                {
                    id = user.Id,
                    userName = user.UserName,
                    fullName = user.FullName,
                    token = tokenString
                });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(nameof(LoginViewModel.Password), ex.Message);
                return BadRequest(ModelState);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            try
            {
                var user = new UserModel
                {
                    UserName = model.UserName,
                    FullName = model.FullName
                };

                var result = await _userService.Add(user, model.Password);

                return Ok(new
                {
                    id = result.Id,
                    userName = result.UserName,
                    fullName = result.FullName,
                    insertDate = result.InsertDate.ToBlogString()
                });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(nameof(UserModel.UserName), ex.Message);
                return BadRequest(ModelState);
            }
        }

        [AllowAnonymous]
        public IActionResult IsNotExisted(string userName)
        {
            var user = _userService.GetByUserName(userName);
            return Json(user != null);
        }
    }
}