﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BlogAPI.Models.User
{
    public class LoginViewModel
    {
        [Required]
        [DisplayName("User Name")]
        public string UserName { get; set; }

        [Required]
        [DisplayName("Password")]
        public string Password { get; set; }
    }
}
