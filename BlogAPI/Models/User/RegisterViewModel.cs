﻿using BlogAPI.Attributes;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BlogAPI.Models.User
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "User Name is required")]
        [RegularExpression(@"^[a-zA-Z0-9_]+$", ErrorMessage = "The User Name must not contain special character and whitespace")]
        [StringLength(32, ErrorMessage = "UserName must be a string with a maximum length of 32")]
        [UserNameNotExist]
        public string UserName { get; set; }

        [DisplayName("Password")]
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

        [DisplayName("Confirm Password")]
        [Required(ErrorMessage = "Confirm Password is required")]
        public string ConfirmPassword { get; set; }

        [DisplayName("Full Name")]
        [Required(ErrorMessage = "Full Name is required")]
        public string FullName { get; set; }
    }
}
