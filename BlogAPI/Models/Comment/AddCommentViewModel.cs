﻿using System.ComponentModel.DataAnnotations;

namespace BlogAPI.Models.Comment
{
    public class AddCommentViewModel
    {
        [Required]
        public int? BlogId { get; set; }

        [Required]
        public string Content { get; set; }
    }
}
