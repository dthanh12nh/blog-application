﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogAPI.Attributes
{
    public class UserNameNotExistAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var userName = value as string;
            if (!string.IsNullOrEmpty(userName))
            {
                var userService = (IUserService)validationContext.GetService(typeof(IUserService));
                if (userService.GetByUserName(userName) != null)
                {
                    return new ValidationResult("User Name is already exist");
                }
            }

            return ValidationResult.Success;
        }
    }
}
