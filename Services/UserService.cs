﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<UserModel> _userRepository;

        public UserService(IRepository<UserModel> userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<UserModel> Authenticate(string userName, string password)
        {
            var user = await _userRepository.FirstOrDefaultAsync(m => m.UserName == userName);

            if (user == null)
            {
                throw new Exception("User Name or Password is incorrect");
            }

            if (VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return user;
            }

            throw new Exception("User Name or Password is incorrect");
        }

        public async Task<UserModel> Add(UserModel model, string password)
        {
            var hashModel = CreatePasswordHash(password);

            var user = new UserModel
            {
                UserName = model.UserName,
                PasswordHash = hashModel.PasswordHash,
                PasswordSalt = hashModel.PasswordSalt,
                FullName = model.FullName
            };

            var result = await _userRepository.Add(user);
            await _userRepository.SaveChanges();

            return result;
        }

        public async Task<UserModel> GetById(int id)
        {
            return await _userRepository.FirstOrDefaultAsync(m => m.Id == id);
        }

        public UserModel GetByUserName(string userName)
        {
            return _userRepository.FirstOrDefault(m => m.UserName == userName);
        }

        private (byte[] PasswordHash, byte[] PasswordSalt) CreatePasswordHash(string password)
        {
            using (var hmac = new HMACSHA512())
            {
                var passwordSalt = hmac.Key;
                var passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                return (passwordHash, passwordSalt);
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
