﻿using Core.Interfaces;
using Core.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Services
{
    public class CommentService : ICommentService
    {
        private readonly IRepository<CommentModel> _commentRepository;

        public CommentService(IRepository<CommentModel> commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public async Task<CommentModel> Add(CommentModel model)
        {
            var result = await _commentRepository.Add(model);
            await _commentRepository.SaveChanges();

            result = await _commentRepository.DbSet.Include(m => m.User).Include(m => m.Blog).FirstAsync(m => m.Id == result.Id);

            return result;
        }
    }
}
