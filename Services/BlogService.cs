﻿using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Core.Interfaces;
using Core.Models;

namespace Services
{
    public class BlogService : IBlogService
    {
        private readonly IRepository<BlogModel> _blogRepository;

        public BlogService(IRepository<BlogModel> blogRepository)
        {
            _blogRepository = blogRepository;
        }

        public async Task<BlogModel> GetById(int id)
        {
            return await _blogRepository.DbSet
                .Include(m => m.Comments)
                .ThenInclude(m => m.User)
                .Include(m => m.User)
                .FirstAsync(m => m.Id == id);
        }

        public (IQueryable<BlogModel> blogs, int totalBlogs) Get(int blogsPerPage, int page)
        {
            var allBlogs = _blogRepository.DbSet.AsQueryable();
            var totalBlogs = allBlogs.Count();
            var blogs = allBlogs.Skip(blogsPerPage * (page - 1)).Take(blogsPerPage);

            return (blogs, totalBlogs);
        }

        public async Task<BlogModel> Add(BlogModel model)
        {
            var result = await _blogRepository.Add(model);
            await _blogRepository.SaveChanges();
            return result;
        }

        public BlogModel Update(BlogModel model)
        {
            var result = _blogRepository.Update(model);
            _blogRepository.SaveChanges();
            return result;
        }

        public BlogModel Delete(int id)
        {
            var model = _blogRepository.DbSet.First(m => m.Id == id);
            _blogRepository.Delete(model);
            _blogRepository.SaveChanges();
            return model;
        }
    }
}
